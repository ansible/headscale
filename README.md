# Headscale

I really tried to make the migration from Tailscale to Headscale work.
But ultimately, this was a failure.

In my opinion, the Headscale project is hampered by a serious lack of available
documentation, making the switch unnecessarily hard. I tried to persevere, but
in the end I could not figure out how to create necessary configuration or
solve misconfigurations. It is really too bad.

It seems the Headscale developers are actively pushing questions and answers
onto their Discord (the issue queue contains multiple closed, but unresolved
issues where the last comment simply says something along the lines of "see Discord").
I guess I can understand how the maintainers arrive at such a policy, but it is
problematic because Discord is an even more closed platform than Github already is.

Headscale is a more open, self-hostable, alternative to Tailscale. As such, it is
not befitting that its documentation is locked behind an even more intrusive service
than Tailscale ever was.

I think this is a very unfortunate by the Headscale project, but can easily be
rectified by either moving to a different groupware service (Matrix, perhaps?),
using Github discussions or similar (although that causes further lock-in to Github,
making it harder to migrate a FOSS git host in the future), or simply allowing
the issue queue to handle questions as well as outright bugs.

I would like to give a big thumbs up to the `headscale-ui` project, which worked
beautifully. Unfortunately, using it becomes rather pointless if one cannot figure
out how to configure the Headscale controller itself.
But assuming Headscale gets its documentation sorted someday, the existence of
`headscale-ui` will be a big part of the reason for me moving back to it.

I come out of this project with improved understanding of how Tailscale works.
But for now, it is back to Tailscale exclusively.

## Create a namespace

A namespace is the equivalent of *tailnet* in Tailscale parlance, i.e.,
your private network. You can define multiple namespaces.

```
root@busan:~
# headscale namespaces create <NAMESPACE>
```

+ https://tailscale.com/kb/1136/tailnet/


## To register a client machine

This piece of advice was hidden in
[Headscale's git history](https://github.com/juanfont/headscale/tree/a63fb6b0075028a365ab21ad3543e5a04c7ba8ec),
but I think it is still relevant:

> If you used `tailscale.com` before in your nodes, make sure you clear
> the `tailscaled` data folder:
> `systemctl stop tailscaled`
> `rm -rf /var/lib/tailscale`
> `systemctl start tailscale`


### Method 1, using normal login

Execute the `tailscale` login command on the client:
```
tailscale up --login-server <YOUR_HEADSCALE_URL>
```

then register the machine on the server using the generated machine key:
```
headscale --namespace <NAMESPACE> nodes register --key <YOUR_MACHINE_KEY>
```

### Method 2, using a pre-authenticated key

> Pre-authenticated keys eventually expire, but they are only needed initially
> when you authenticate the machine. After that the machine should stay authenticated
> until it logs out or is manually expired.
> https://github.com/juanfont/headscale/issues/562#issuecomment-1153157016

Generate a preauthenticated key (default expiration time is 1 h):
```
headscale --namespace <NAMESPACE> preauthkeys create --reusable --expiration 24h
```

Register the client to our Headscale server using the generated key:
```
tailscale up --login-server <YOUR_HEADSCALE_URL> --authkey <YOUR_AUTH_KEY>
```

+ https://github.com/juanfont/headscale/blob/main/docs/running-headscale-linux.md#register-machine-using-a-pre-authenticated-key


### Register an Android client

Open the Tailscale app, click the three-dot menu repeatedly until the option
**Change server** appears. Change the URL to that of our Headscale instance,
save and restart the app.

Upon next sign-in, the app will show a "Machine registration" page
displaying a machine key. Use it to register the device (on the Headscale server):

```
root@busan:~
# headscale -n <NAMESPACE> nodes register --key <YOUR_MACHINE_KEY>
```

+ https://github.com/juanfont/headscale/blob/main/docs/android-client.md


## Other Ansible roles

+ https://github.com/kazauwa/ansible-role-headscale
+ https://noa.mornie.org/eriol/ansible-collection-kit/src/branch/main/roles/headscale [blog post](https://mornie.org/blog/using-headscale-for-your-home-networks/)


## Refs

+ https://tailscale.com/blog/opensource
+ https://github.com/juanfont/headscale/blob/main/docs/running-headscale-linux.md
+ https://github.com/juanfont/headscale/blob/main/docs/reverse-proxy.md
+ https://techoverflow.net/2022/02/02/headscale-nginx-reverse-proxy-config
+ [Enable Android users to use Tailscale clients with Headscale](https://github.com/tailscale/tailscale-android/pull/45) (long-press the triple-dot menu)
+ [Any web UI planned for Headscale? #234](https://github.com/juanfont/headscale/issues/234)
+ https://github.com/gurucomputing/headscale-ui
+ [Override local DNS option for Pihole and Adguardhome? #280](https://github.com/juanfont/headscale/issues/280)
+ [Is subnet routing supported? #117](https://github.com/juanfont/headscale/issues/117)
+ [Enable changing device name #195](https://github.com/juanfont/headscale/issues/195)
