---

# After running all blocks in this file, we may expect to have the following variables:
# + headscale_version, containing version string, e.g., 0.16.4
# + headscale_download_url, containing URL to Headscale binary, e.g.,
#   https://github.com/juanfont/headscale/releases/download/v0.16.4/headscale_0.16.4_linux_amd64


- name: "Find URL of headscale v{{ headscale.version }} executable"
  when: headscale.version != "latest"
  block:

    # https://api.github.com/repos/juanfont/headscale/releases/tags/v0.16.4
    - name: "Query Github API for headscale v{{ headscale.version }}"
      run_once: true
      delegate_to: localhost
      ansible.builtin.uri:
        method: GET
        status_code: 200
        url: "https://api.github.com/repos/{{ headscale.gh_slug }}/releases/tags/v{{ headscale.version }}"
      register: headscale_uri

    # note, we need to append "first" to get a single element and not a list (of one element, but still a list)
    - ansible.builtin.set_fact:
        headscale_download_url: >-
          {{ headscale_uri.json.assets |
             json_query('[*].browser_download_url') |
             select('search', 'linux_amd64$') |
             first }}

    - ansible.builtin.set_fact:
        headscale_version: "{{ headscale.version }}" # 0.15.0
  # END OF BLOCK


- name: Find latest Headscale release
  when: headscale.version == "latest"
  block:

    - name: Query Github API for the latest headscale release
      run_once: true
      delegate_to: localhost
      ansible.builtin.uri:
        method: GET
        status_code: 200
        url: "https://api.github.com/repos/{{ headscale.gh_slug }}/releases/latest"
      register: headscale_uri

    # strip the first character to get version string without preceding "v"
    - name: Find latest headscale version
      ansible.builtin.set_fact:
        headscale_version: "{{ headscale_uri.json.tag_name[1:] }}" # 0.16.4

    # "assets" is a list of dicts, each one contains a "browser_download_url"
    # jmespath must be installed (on the controller) to use the json_query filter
    - ansible.builtin.set_fact:
        headscale_download_urls: >-
          {{ headscale_uri.json.assets |
             json_query('[*].browser_download_url') }}

    # now we need to extract the correct file from the list, "linux_amd64$" should pick just the one
    - ansible.builtin.set_fact:
        headscale_download_url: >-
          {{ headscale_download_urls |
             select('search', 'linux_amd64$') |
             first }}
  # END OF BLOCK
