# So, this is not working...

I can't SSH to any node on my tailnet from `asks2`. What's up with that?

## Proxy?

I am using Apache with a home-baked config instead of the officially supported NGINX...

+ [Problems with headscale behind nginx proxy](https://github.com/juanfont/headscale/issues/56)

**No, my Apache vhost seems to work fine.**


## DERP?

On the clients, `tailscale status` keeps showing:
```
Health check: not connected to home DERP region 14
```
before listing the nodes.
I have the STUN UDP port port-forwarded from the router, and I can't find any mentions
of `derp` in the headscale logs with `log_level: debug`.

I am going to admit that I have no idea how DERP is supposed to work, and have
decided to disable it in `config.yaml` and also remove the STUN port port-forward.

Different health check warning after disabling DERP:
```
Health check:
  - router: setting up filter/ts-input: running [/usr/sbin/ip6tables -t filter -N ts-input --wait]:
    exit status 3: modprobe: FATAL: Module ip6_tables not found in directory /lib/modules/5.15.0-43-generic
  ip6tables v1.8.4 (legacy): can't initialize ip6tables table `filter': Table does not exist (do you need to insmod?)
  Perhaps ip6tables or your kernel needs to be upgraded.
```
Only observed on `bilbeis` so far, which is LXC container Ubuntu 20.04, tailscale 1.32.0 (go1.19.2-ts3fd24dee31).
Could this be a container/modprobe issue? Or is it because of the slightly older Ubuntu version?

Wait a minute, this could have some basis, check this out:
```
taha@bilbeis:~
$ sudo ip6tables -L
modprobe: FATAL: Module ip6_tables not found in directory /lib/modules/5.15.0-43-generic
ip6tables v1.8.4 (legacy): can't initialize ip6tables table `filter': Table does not exist (do you need to insmod?)
Perhaps ip6tables or your kernel needs to be upgraded.
```
(the same is **not** observed on `x230t`).

Let's investigate further (remember, this is `bilbeis`, an Ubuntu 20.04 unprivileged LXC container on `luxor`).
```
taha@bilbeis:~
$ type -a iptables
iptables is /usr/sbin/iptables
iptables is /sbin/iptables
```

`/lib/modules/` is **empty** on the container, but obviously not on the host:
```
root@luxor:~
# tree -d -L 2 /lib/modules/
/lib/modules/
├── 5.15.0-43-generic
│   ├── build -> /usr/src/linux-headers-5.15.0-43-generic
│   ├── initrd
│   ├── kernel
│   └── vdso
└── 5.15.0-47-generic
    ├── build -> /usr/src/linux-headers-5.15.0-47-generic
    ├── initrd
    ├── kernel
    └── vdso
# ll /lib/modules/$(uname -r)/kernel/net/ipv4/netfilter/ip*table
-rw-r--r-- 1 root root 14K  jul 12 10:51 iptable_filter.ko
-rw-r--r-- 1 root root 11K  jul 12 10:51 iptable_mangle.ko
-rw-r--r-- 1 root root 11K  jul 12 10:51 iptable_nat.ko
-rw-r--r-- 1 root root 12K  jul 12 10:51 iptable_raw.ko
-rw-r--r-- 1 root root 9.7K jul 12 10:51 iptable_security.ko
-rw-r--r-- 1 root root 56K  jul 12 10:51 ip_tables.ko
```

+ https://superuser.com/questions/813323/iptables-module-ip-tables-not-found-for-root
+ https://stackoverflow.com/questions/3140478/fatal-module-not-found-error-using-modprobe

Let's list the loaded kernel modules (only ip-related), first on the container:
```
taha@bilbeis:~
$ grep -ir ip /proc/modules
iptable_nat 16384 0 - Live 0x0000000000000000
iptable_filter 16384 1 - Live 0x0000000000000000
ipt_REJECT 16384 2 - Live 0x0000000000000000
nf_reject_ipv4 16384 1 ipt_REJECT, Live 0x0000000000000000
nf_nat 49152 4 iptable_nat,nft_masq,xt_MASQUERADE,nft_chain_nat, Live 0x0000000000000000
nf_defrag_ipv6 24576 1 nf_conntrack, Live 0x0000000000000000
nf_defrag_ipv4 16384 1 nf_conntrack, Live 0x0000000000000000
dm_multipath 40960 0 - Live 0x0000000000000000
ipmi_devintf 20480 0 - Live 0x0000000000000000
ipmi_msghandler 122880 1 ipmi_devintf, Live 0x0000000000000000
ip_tables 32768 2 iptable_nat,iptable_filter, Live 0x0000000000000000
x_tables 53248 11 xt_mark,iptable_nat,iptable_filter,xt_comment,xt_CHECKSUM,xt_MASQUERADE,xt_conntrack,ipt_REJECT,xt_tcpudp,nft_compat,ip_tables, Live 0x0000000000000000
multipath 20480 0 - Live 0x0000000000000000
```
Note that both `iptable_filter` and `ip_tables` are listed.

Second, on the host:
```
root@luxor:~
# grep -ir ip /proc/modules
iptable_nat 16384 0 - Live 0xffffffffc1a82000
iptable_filter 16384 1 - Live 0xffffffffc1a7a000
ipt_REJECT 16384 2 - Live 0xffffffffc121b000
nf_reject_ipv4 16384 1 ipt_REJECT, Live 0xffffffffc11fa000
nf_nat 49152 4 iptable_nat,nft_masq,xt_MASQUERADE,nft_chain_nat, Live 0xffffffffc1203000
nf_defrag_ipv6 24576 1 nf_conntrack, Live 0xffffffffc11bf000
nf_defrag_ipv4 16384 1 nf_conntrack, Live 0xffffffffc11ba000
dm_multipath 40960 0 - Live 0xffffffffc0bd9000
ipmi_devintf 20480 0 - Live 0xffffffffc0c8e000
ipmi_msghandler 122880 1 ipmi_devintf, Live 0xffffffffc0c68000
ip_tables 32768 2 iptable_nat,iptable_filter, Live 0xffffffffc0581000
x_tables 53248 11 xt_mark,iptable_nat,iptable_filter,xt_comment,xt_CHECKSUM,xt_MASQUERADE,xt_conntrack,ipt_REJECT,xt_tcpudp,nft_compat,ip_tables, Live 0xffffffffc0551000
multipath 20480 0 - Live 0xffffffffc033c000
```
Container and host output is identical (apart from memory addresses).

So the problem is not that the [kernel module is not loaded on the host](https://ursrig.com/lxc-iptables-error-when-not-loading-kernel-module).

~~I think `bilbeis` needs to be a privileged container at the very least, perhaps~~
~~even with security nesting and other config.~~

+ https://stackoverflow.com/a/63444602/1198249
+ https://github.com/corneliusweig/kubernetes-lxd/blob/master/README-k3s.md#starting-your-lxc-container

> In the Tailscale Linux client (since v1.12) the **userspace-networking mode**
> handles operation in container environments which do not have `CAP_NET_ADMIN` set.
> https://github.com/tailscale/tailscale/issues/504#issuecomment-883921653

+ https://tailscale.com/kb/1112/userspace-networking/

> To be able to tinker with `iptables` rules in a container, the container needs
> the `NET_ADMIN` capability.
> https://github.com/moby/moby/issues/33605#issuecomment-307361421

Let's give the unprivileged container `NET_ADMIN` capability *and* access to `/dev/net/tun`.
How?

But are both of these additions really necessary? The `kuwait` container
(which runs Tailscale client connected to Tailscale controller) works just fine
without any of these additions.

Also, `capsh --print` shows that `cap_net_admin` is available for all containers
I have tested it on. This supports (like some comment to one of the linked pages)
that recent versions of LXC use some clever programming to allow `cap_net_admin`
even for unprivileged containers.

+ https://man7.org/linux/man-pages/man7/capabilities.7.html
+ https://linuxcontainers.org/lxc/manpages/man5/lxc.container.conf.5.html

Aargh. I missed something! Remember how `/lib/modules/` was empty in the container?
I thought this was the case for all LXC containers, and neglected to check any
other container.
Turns out this directory is mirrored on all containers that **use the same Ubuntu
release as the host**, and empty on containers that use older releases.
This might explain why the Tailscale client on `kuwait` runs without any complaints.

**So, we should re-provision `bilbeis` with Ubuntu Jammy and reassess.**

We now have `bilbeis` re-provisioned with Ubuntu 22.04.
Weirdly enough, the `/lib/modules/` directory is still empty, but in any case
the Tailscale health check no longer prints any notices or warnings.
**Issue fixed.**



## Outdated Tailscale client?

May have been contributing, but even with the latest version the above issues
still manifested.



## Cosmetic issue with tailscale status showing IPv6 and not IPv4

Also, there is a cosmetic issue in that `tailscale status` shows IPv6 addresses
instead of IPv4:
```
$ tailscale status
fd7a:115c:a1e0::5 bilbeis     home         linux   -
fd7a:115c:a1e0::6 alexandria  home         linux   -
```
This issue may be related to https://github.com/juanfont/headscale/issues/148.
